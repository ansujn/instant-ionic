angular.module('songhop.services', [])
.service('userService', function() {
  var userService = this;
  userService.sharedObject = {};

  userService.getNumber = function(){
     return userService.sharedObject.Number;
  }

  userService.setNumber = function(value){
     userService.sharedObject.Number = value;
  }

  userService.getName = function(){
     return userService.sharedObject.Name;
  }

  userService.setName = function(value){
     userService.sharedObject.Name = value;
  }
});
