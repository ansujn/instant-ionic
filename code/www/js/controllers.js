angular.module('songhop.controllers', ['ionic', 'songhop.services'])


/*
Controller for the discover page
*/
.controller('DiscoverCtrl', function($scope , $location, $ionicPopup, userService, $cordovaOauth, $localStorage,$state) {
  $scope.number = "";
  $scope.login = function() {
        $cordovaOauth.facebook("1611466895801713", ["email"]).then(function(result) {
            $localStorage.accessToken = result.access_token;
            $location.path("/profile");
        }, function(error) {
            alert("There was a problem signing in!  See the console for logs");
        });
    };
  $scope.go = function ( path,number ) {
      console.log(number);
      userService.setNumber(number);
      $state.go('otp');
    };

 // An alert dialog
 $scope.showAlert = function(number) {
   var alertPopup = $ionicPopup.alert({
     title: 'Error!!',
     template: !number?'Fill correct info.':'Please aggree to T&C.'
   });
   alertPopup.then(function(res) {
     console.log('Something went wrong.');
   });
 };
})


/*
Controller for the favorites page
*/
.controller('FavoritesCtrl', function($scope) {

})


/*
Controller for the otp page
*/
.controller('OtpCtrl', ['$scope', '$location', 'userService', '$state', function($scope,$location, userService, $state) {
    $scope.go = function ( path ) {
      $location.path( path );
      console.log(path);
    };

    $scope.stateName = $state.current.name;
    $scope.userService = userService;
}])


/*
Controller for Profile
*/
.controller('ProfileCtrl',function($scope, $http, $localStorage, $location,$ionicPopup, $timeout, $ionicModal, userService) {
  // Triggered on a button click, or some other target

   	$scope.user={
   		name: 'Mr. Raj Malhotra',
   		dob: '20/03/1883',
   		face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png',
   		pan: 'AEZP132401',
   		income: '1,00,0000'
   	};
     userService.setName($scope.user.name);
     console.log(userService.getName());
    $scope.init = function() {
        if($localStorage.hasOwnProperty("accessToken") === true) {
            $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: $localStorage.accessToken, fields: "id,name,gender,picture", format: "json" }}).then(function(result) {
                $scope.user.name = result.data.name;
                $scope.user.face = result.data.picture.data.url;
                userService.setName(result.data.name);
            }, function(error) {
                alert("There was a problem getting your profile.  Check the logs for details.");
                console.log(error);
            });
        } else {
            alert("Not signed in");
        }
    };

    if($localStorage.hasOwnProperty("accessToken") === true){
      $scope.init();
    }
    $scope.showPopup = function() {
      $scope.data = {}
  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    templateUrl: 'templates/edit_name.html',
    title: '<b>Edit Name & Pic</b>',
    cssClass: 'edit-name',
    scope: $scope,
    buttons: [
      { text: 'CANCEL' },
      {
        text: '<font color="#D10A7B">SUBMIT</font>',
        onTap: function(e) {
          if (!$scope.data.username) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
            console.log($scope.data.username);
            $scope.user.name = $scope.data.username;
            return $scope.data.username;
          }
        }
      }
    ]
  });
  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });
  $timeout(function() {
    if (!$scope.data.username) {
      myPopup.close(); //close the popup after 5 seconds if nothing is entered.
    }
  }, 5000);
 };


  $scope.go = function ( path ) {
      $location.path( path );
      console.log(path);
    };
})
/*
Controller for our Bank Select Page
*/
.controller('BankCtrl', function($scope,$location,$ionicPopup, $timeout,$ionicModal) {
  $scope.go = function ( path ) {
      $location.path( path );
      console.log(path);
    };


  // Triggered on a button click, or some other target
$scope.showPopup = function() {
  $scope.data = {};
  console.log("abc");
  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    templateUrl: 'templates/credentials.html',
    title: '<b>Credentials</b>',
    cssClass: 'edit-name',
    scope: $scope,
    buttons: [
      { text: 'CANCEL' },
      {
        text: '<font color="#D10A7B">SUBMIT</font>',
        onTap: function(e) {
          if (!$scope.data.customerid) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {

            console.log($scope.data.customerid);
            console.log($scope.data.ipin);
            $location.path( '/waiting' );
            // return $scope.data.customerid;
          }
        }
      }
    ]
  });
  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });
  $timeout(function() {
    if (!$scope.data.customerid) {
      myPopup.close(); //close the popup after 5 seconds if nothing is entered.
    }
  }, 5000);
 };
})

.controller('WaitingCtrl', function($scope,$location,userService) {
  $scope.go = function ( path ) {
      $location.path( path );
      console.log(path);
    };
    console.log(userService.getName());
    $scope.user = userService;
})
/*
Controller for our tab bar
*/
.controller('TabsCtrl', function($scope) {

});
